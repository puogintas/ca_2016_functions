<?php

function ca_2016_functions_form_i18n_string_locale_translate_edit_form_alter(&$form, $form_state) {
    unset($form['#validate']);
}

function ca_2016_functions_field_widget_form_alter(&$element, &$form_state, $context) {
	if($form_state['build_info']['form_id'] == 'cxu_chromecast_entityform_edit_form') {
		if($context['field']['field_name'] == 'field_location_address') { $element['country']['#weight'] = 600; }
		if($context['field']['field_name'] == 'field_serial_3') { $element['value']['#maxlength'] = 4; }
		if($context['field']['field_name'] == 'field_serial_4') { $element['value']['#maxlength'] = 4; }
	}
	
	if($form_state['build_info']['form_id'] == 'minx_centre_grille_order_centre_entityform_edit_form' || $form_state['build_info']['form_id'] == 'dacmagic_psu_replacement_program_entityform_edit_form') {
		if($context['field']['field_name'] == 'field_postal_address') { $element['country']['#weight'] = 600; }
	}
}

function ca_2016_functions_form_alter(&$form, &$form_state, $form_id) {

	if($form_id == 'customer_reviews_node_form') {
		if(empty($form['nid']['#value'])) {
			//Disabling ADMIN level fields references...
			$form['field_product_reviewed']['#access'] = FALSE;
			$form['field_product_referenced']['#access'] = FALSE;
			$form['field_c_reviews_admin_status']['#access'] = FALSE;
			$form['field_c_reviews_admin_zendesk']['#access'] = FALSE;
			$form['field_c_reviews_admin_confirm']['#access'] = FALSE;
			$form['field_c_reviews_reply']['#access'] = FALSE;
			
			//Changing Submit button
			$form['actions']['submit']['#value'] = t('Submit');
			$form['actions']['submit']['#prefix'] = '<div class="form_button--wrapper">';
			$form['actions']['submit']['#suffix'] = '</div>';
			
			//Captcha wraps
			$form['captcha']['#prefix'] = '<div class="form__review__step—submit">';
			$form['captcha']['#suffix'] = '';
			
			//Actions wraps
			$form['actions']['#prefix'] = '';
			$form['actions']['#suffix'] = '</div>';
			
			
			$form['extra_step1'] = array(
				'#suffix' => '<div class="form__review__step—bans-wrapper"><a href="#" class="form__review__step—buton-next">' . t('Next') . '</a></div>',
			);
			$form['#group_children']['extra_step1'] = 'group_half4';
			
			$form['extra_step2'] = array(
				'#suffix' => '<div class="form__review__step—bans-wrapper"><a href="#" class="form__review__step—buton-prev">' . t('Previous') . '</a><a href="#" class="form__review__step—buton-next">' . t('Next') . '</a></div>',
			);
			$form['#group_children']['extra_step2'] = 'group_wrapp2';
			
			$form['extra_step3'] = array(
				'#suffix' => '<div class="form__review__step—bans-wrapper"><a href="#" class="form__review__step—buton-prev">' . t('Previous') . '</a></div>',
			);
			$form['#group_children']['extra_step3'] = 'group_wrapp';
			
		} else {
			$form['actions']['submit']['#value'] = t('Edit');
			$form['actions']['submit']['#prefix'] = '<div class="form_button--wrapper">';
			$form['actions']['submit']['#suffix'] = '</div>';
			$form['actions']['delete']['#access'] = FALSE;
		}
	}
	
	if($form_id == 'dacmagic_psu_replacement_program_entityform_edit_form' || $form_id == 'minx_centre_grille_order_centre_entityform_edit_form' || $form_id == 'product_registration_entityform_edit_form') {
		switch($form_id) {
			case 'dacmagic_psu_replacement_program_entityform_edit_form':
				$field_name = 'field_serial_dacpsu';
				break;
			case 'minx_centre_grille_order_centre_entityform_edit_form':
				$field_name = 'field_serial_minx';
				break;
			case 'product_registration_entityform_edit_form':
				$field_name = 'field_serial';
				break;
		}
		
		if(empty($form[$field_name])) {	return; }
		$field_language = $form[$field_name]['#language'];
		$max_delta = $form[$field_name][$field_language]['#max_delta'];
		unset($form[$field_name][$field_language][$max_delta]);
		//to be investigated
		$form[$field_name][$field_language]['#max_delta'] = 0;
	}
}

?>