<?php

use KamranAhmed\Geocode\Geocode;

function ca_2016_functions_rules_action_info() {
	$actions = array();
	
	$actions['geocode_address'] = [
		'label' => t('Geocode Dealer Location address'),
		'group' => t('CA functions'),
		'parameter' => [
			'dealer' => [
				'type' => 'node',
				'label' => t('Dealer Location'),
			],
		],
	];
	
	$actions['generate_json'] = array(
		'label' => t('Generating JSON object for tile pages'),
		'group' => t('CA functions'),
		'parameter' => array(
			'json_options' => array(
				'type' => 'text',
				'label' => t('Page to generate'),
				'description' => t('Which page JSON to generate'),
				'options list' => 'json_options',
				'restriction' => 'input',
			),
		),
		'provides' => array(
			'error' => array(
				'type' => 'text',
				'label' => t('erorr'),
			),
		),
	);
	
	return $actions;
}

function json_options() {
	return array(
		'front' => 'Front Page',
		'about_us' => 'About us Page',
		'musical' => 'Musical Momments',
		'blog' => 'Front Blog Page',
	);
}

function generate_json($json_options) {
	switch($json_options) {
		case 'front': $url = 'tiles/front-page/json'; $name = 'front_'; break;
		case 'about_us': $url = 'json/about-us'; $name = 'about_us_'; break;
		case 'musical': $url = 'tiles/musical_moments/json'; $name = 'musical_moments_'; break;
		case 'blog': $url = 'tiles/blogs/json'; $name = 'blog_'; break;
	}
	
	global $user;
	$original_user = $user;
	$old_state = drupal_save_session();
	drupal_save_session(FALSE);
	$user = user_load(1);
	
	$lang = array(
		'en' => '',
		'de' => 'de/',
		'fr' => 'fr/',
		'es' => 'es/',
		'zh-hans' => 'zh/',
		'zh-hant' => 'zh-hant/',
		'pl' => 'pl/',
	);
	
	try {
		foreach($lang as $key => $value) {
			$json = file_get_contents('https://www.cambridgeaudio.com/' . $value . $url);
			file_unmanaged_save_data($json, 'public://front/json/' . $name . $key . '.json', FILE_EXISTS_REPLACE);
		}
	} catch (Exception $e) {
		$error = $e;
	}

	$user = $original_user;
	drupal_save_session($old_state);

	return array(
        'error' => !empty($error) ? '$error' : NULL,
    );
}

function geocode_address($dealer) {
	global $user;
	$original_user = $user;
	$old_state = drupal_save_session();
	drupal_save_session(FALSE);
	$user = user_load(1);
	
	/* defining Geocode object*/
	$geocode = new Geocode('AIzaSyB3t0WWyON8oyfMbe5robL3fOdxvhXZAKE');
	
	$dealer = entity_metadata_wrapper('node', node_load($dealer->nid));
	
	/* creating address array for use */
	$address = [];
	
	/* if proper address field have been used */
	$addressData = $dealer->field_location_address;
	$address[] = $addressData->organisation_name->value();
	$address[] = $addressData->thoroughfare->value();
	$address[] = $addressData->premise->value();
	$address[] = $addressData->locality->value();
	$address[] = $addressData->administrative_area->value();
	$address[] = $addressData->postal_code->value();
	$address[] = country_load($addressData->country->value())->name;
	array_filter($address);
	
	/* getting string of address */
	$location = $geocode->get(implode(', ', $address));

	/* if geocoding did not fail save data to field...  */
	if(!empty($location->getLongitude()) && !empty($location->getLongitude())) {
		/* preping geofield data for saving.. */
		$geofield_data = geofield_compute_values([
			'lat' => $location->getLatitude(),
			'lon' => $location->getLongitude(),
		], GEOFIELD_INPUT_LAT_LON);

		$dealer->field_location_data->set($geofield_data);
		$dealer->save();
	}
	
	$user = $original_user;
	drupal_save_session($old_state);
}

function multiExplode($delimiters, $string) {
    return explode($delimiters[0],strtr($string,array_combine(array_slice($delimiters,1),array_fill(0,count($delimiters)-1,array_shift($delimiters)))));
}

function ca_2016_functions_rules_condition_info() {
	$conditions = [];
	
	$conditions['validate_email_unique'] = [
		'label' => t('Is email unique in this Form?'),
		'group' => t('CA Conditions'),
		'arguments' => [
			'submission' => [
				'type' => 'entityform',
				'label' => t('current Submission')
			],
		],
		'module' => 'ca_2016_functions',
	];
	
	return $conditions;
}

function validate_email_unique($submission) {
	global $user;
	$original_user = $user;
	$old_state = drupal_save_session();
	drupal_save_session(FALSE);
	$user = user_load(1);
	
	if(empty($submission)) { return FALSE; }
	
	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type', 'entityform');
	$query->entityCondition('bundle', $submission->type);
	$query->fieldCondition('field_email', 'email', $submission->field_email['und'][0]['email'], '=');

	$result = intval($query->count()->execute());
	
	//clossing Fake admin session
    $user = $original_user;
	drupal_save_session($old_state);
	
	return ($result == 0) ? TRUE : FALSE;
}

?>