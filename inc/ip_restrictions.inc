<?php

function uk_vistor_only() {
	$country = $_SERVER['HTTP_CF_IPCOUNTRY'];
	if($country == 'GB') { return TRUE; }
}

function de_vistor_only() {
	$country = $_SERVER['HTTP_CF_IPCOUNTRY'];
	if($country == 'DE') { return TRUE; }
}

function hk_vistor_only() {
	$country = $_SERVER['HTTP_CF_IPCOUNTRY'];
	if($country == 'HK') { return TRUE; }
}

function us_vistor_only() {
	$country = $_SERVER['HTTP_CF_IPCOUNTRY'];
	if($country == 'US') { return TRUE; }
}

function cn_vistor_only() {
	$country = $_SERVER['HTTP_CF_IPCOUNTRY'];
	if($country == 'CN') { return TRUE; }
}

function vistor_country() {
	return $_SERVER['HTTP_CF_IPCOUNTRY'];
}

?>