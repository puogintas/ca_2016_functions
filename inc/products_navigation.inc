<?php

function products_navigation() {
	global $language;
	
	$json = [];
	
	$vocab = taxonomy_vocabulary_machine_name_load('header_navigation');
	$allTerm = taxonomy_get_tree($vocab->vid);
	
	$tree = taxonomy_tree_to_nested_array($allTerm);
	
	foreach($tree as &$term) {
		if(!empty($term)) {
			$local_term = i18n_taxonomy_localize_terms($term['data']);
			$children = [];
			if(!empty($term['children'])) {
				foreach($term['children'] as &$child) {
					$childObject = navigation_term_format($child, $language);
					$children[] = $childObject;
				}

			}
			$termObject = (object) [
				'tid' => $term['data']->tid,
				'title' => $local_term->name,
				'machine_title' => preg_replace('@[^a-z0-9-]+@','_', strtolower($term['data']->name)),
				'child-category' => $children,
			];
			$json[] = $termObject;
		}
	}
	
	return $json;
}

function navigation_term_format($term, $language) {
	$local_term = i18n_taxonomy_localize_terms($term['data']);
	$products = [];
	
	if(empty($term['children'])) {
		$query = new EntityFieldQuery();
		$query->entityCondition('entity_type', 'node');
		$query->entityCondition('bundle', 'product_new');
		$query->propertyCondition('status', NODE_PUBLISHED);
		$query->fieldCondition('field_belongs_to_category', 'target_id', $term['data']->tid, '=');
		$results = $query->execute();
		if(!empty($results)) {
			foreach($results['node'] as &$product_id) {
				$product = node_load($product_id->nid);
				$products[] = [
					'title' => $product->field_n_display_title[$language->language][0]['value'],
					'strapline' => $product->field_n_strapline[$language->language][0]['value'],
					'nid' => $product->nid,
					'url' => langprefix($language) . drupal_get_path_alias('node/' . $product->nid),
				];
			}
		}
	}
	
	$termObject = [
		'tid' => $term['data']->tid,
		'title' =>  $local_term->name,
		'machine_title' => preg_replace('@[^a-z0-9-]+@','_', strtolower($term['data']->name)),
		'products' => $products,
	];
	
	return $termObject;
}

/*
* Takes a taxonomy tree as returned by taxonomy_get_tree() and returns a
* nested array of taxonomy terms.
*
* @param array $tree
*
* @return array
*   A multi-dimensional array where each element consists of two keys:
*   - (stdClass) data: the unaltered taxonomy term
*   - (array) children: an array of child terms in the same array format
*/

function taxonomy_tree_to_nested_array($tree) {
	// Put all of the terms into a new array and map their each term's children
	// in a temporary _children array
	$terms = array();
	foreach ($tree as $term) {
		$terms[$term->tid] = array('data' => $term, 'children' => array(), '_children' => array());
		foreach ($term->parents as $tid) {
			// Check for empty parent
			if (!empty($tid)) {
				$terms[$tid]['_children'][$term->tid] = $term->tid;
			}
		}
	}

	while (TRUE) {
		$break = TRUE;
		foreach ($terms as $tid => &$term) {
			// If this term has children, it can't be moved yet. If it has no
			// parents it also can't be moved
			if (!empty($term['_children']) || empty($term['data']->parents[0])) {
				continue;
			}

			// Remove the temporary children array, it's now empty and of no use
			unset($term['_children']);

			// Move the term under it's parents.
			foreach ($term['data']->parents as $parent) {

				// Check for empty parent
				if (!empty($parent)) {
					// Put term under parent, and update the parent's temporary children
					// array so we know this child has been processed
					$terms[$parent]['children'][] = $term;
					unset($terms[$parent]['_children'][$tid]);

					// As we have altered the array, we need to loop through it again
					$break = FALSE;

				}
			}

			// Remove this term from the base array
			unset($terms[$tid]);

			// If the temporary children array for this term is now empty, remove
			// it. We do this check so that it's removed from the top level terms
			if (empty($term['_children'])) {
				unset($term['_children']);
			}

		}

		if ($break) {
			break;
		}

	}

	// Re-key the array to remove the term ID based indexes from the top level
	return array_values($terms);
}

/* Function to get current term depth level root is 1 */
function get_term_depth($tid) {
	$limit = 5;
	$depth = 0;
	while ($parent = db_select('taxonomy_term_hierarchy', 't') ->condition('tid', $tid, '=') ->fields('t') ->execute() ->fetchAssoc()) {
		$depth ++;
		$tid = $parent['parent'];
		if ($depth > $limit) {
			break;
		}
	}
	return $depth;
}


/* function to sort nested array by Term weight */
function taxonomy_term_tree_weight_sort($Hierarchy) {
	if(!empty($Hierarchy[0]['children'])) {
		$tmp = [];
		foreach($Hierarchy[0]['children'] as $key => $term) {
			$tmp[] = $term['data']->weight;
			if(!empty($term['children'])) {
				$chld_tmp = [];
				foreach($term['children'] as $child_key => $child) {
					$chld_tmp[] = $child['data']->weight;
					
					if(!empty($child['children'])) {
						$chld_2_tmp = [];
						foreach($child['children'] as $child) {
							$chld_2_tmp[] = $child['data']->weight;
						}
						array_multisort($chld_2_tmp, $Hierarchy[0]['children'][$key]['children'][$child_key]['children']);
					}
				}
				array_multisort($chld_tmp, $Hierarchy[0]['children'][$key]['children']);
			}
		}
		array_multisort($tmp, $Hierarchy[0]['children']);
	}
	
	return $Hierarchy;
}


?>