<?php

use KamranAhmed\Geocode\Geocode;

/**
* add custom CSS to all admin pages if user logged is one of BDM's
* @private
* @method ca_2016_functions_preprocess_page
* @param {Object} $vars
* @return {Object} description
*/
function ca_2016_functions_preprocess_page($vars) {
	$user = $vars['user'];
	if(in_array('BDM', $user->roles) && arg(0) == 'admin') {
		drupal_add_css(drupal_get_path('module', 'ca_2016_functions') . '/inc/bdm.css');
	}
}

/**
* Creates Dealer JSON object used by find a dealer map
* @private
* @method dealers_json_render
* @return {Object} json ecoded object
*/
function dealers_json_render() {
//	header('Content-Type: application/json; charset=utf-8');
	drupal_add_http_header('Content-Type', 'application/json; charset=utf-8;');
	
	/* gets all dealer json file*/
	echo get_dealer_json();
	
	/* returns null making page cacheble */
	return NULL;
}

function dealers_json_render_special($uid) {
  drupal_add_http_header('Content-Type', 'application/json; charset=utf-8;');

  /* trigger query to get all currect dealer locations */
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->entityCondition('bundle', 'dealer');
//  $query->fieldCondition('field_location_address', 'country', '', '<>'); // making sure all dealers have address field
  if(!empty($uid) && $uid != 0) { $query->propertyCondition('uid', $uid); }
  $query->propertyCondition('status', NODE_PUBLISHED);
  $results = $query->execute();

  $dealers = [];

  $nids = array_keys($results['node']);
  $nodes = node_load_multiple($nids);

  foreach($nodes as $retailer) {
    $retailer =  entity_metadata_wrapper('node', $retailer);

    /* Dealer Type */
    $storeType = [];
    if(!empty($retailer->field_store_type)) {
      foreach($retailer->field_store_type as $type) {
        $storeType[] = (object) [
          'name' => $type->name->value(),
          'tid' => $type->tid->value(),
        ];
      }
    }

    if(!empty($retailer->field_website->value())) {

      $field_url = field_view_field('node', $retailer->value(), 'field_website', [
        'label' => 'hidden',
        'type' => 'link_plain',
      ]);

      $website = (object) [
        'url' => $field_url['#items'][0]['url'],
        'title' => $retailer->field_website->value()['title'],
      ];

    }

    $dealers[] = (object) [
      'title' => $retailer->title->value(),
      'address' => (object) $retailer->field_location_address->value(),
      'store_type' => $storeType,
      'website' => !empty($website) ? $website : [],
      'contact' => [
        'tel' => $retailer->field_telephone->value(),
        'fax' => $retailer->field_fax->value(),
        'email' => $retailer->field_email->value(),
      ],
      'logo' => !empty($retailer->field_logo->value()) ? [
        'uri' => file_create_url($retailer->field_logo->value()['uri']),
        'name' => $retailer->field_logo->value()['filename'],
      ] : NULL,
      'image' => !empty($retailer->field_dealer_image->value()) ? [
        'uri' => file_create_url($retailer->field_dealer_image->value()['uri']),
        'name' => $retailer->field_dealer_image->value()['filename'],
      ] : NULL,
      'field_dealer_info' => $retailer->field_dealer_info->value(),
      'field_opening_hours' => $retailer->field_opening_hours->value(),
      'enhanced' => $retailer->field_enable_store_page->value(),
      'field_legacy_address' => $retailer->field_legacy_address->value(),
      'owner' => (object) [
        'email' => $retailer->author->mail->value(),
        'uid' => $retailer->author->uid->value(),
      ],
    ];
  }

  $json = [
    'data' => $dealers,
  ];

  echo html_entity_decode(json_encode($json, JSON_UNESCAPED_UNICODE));

  /* returns null making page cacheble */
  return NULL;
}

/**
* get data into JSON format
* @private
* @method get_dealer_json
* @return {Object} description
*/
function get_dealer_json() {
	/* defineing GEOCODE object */
	$geocode = new Geocode('AIzaSyB3t0WWyON8oyfMbe5robL3fOdxvhXZAKE');

	/* trigger query to get all currect dealer locations */
	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type', 'node');
	$query->entityCondition('bundle', 'dealer');
	$query->propertyCondition('status', NODE_PUBLISHED);
	$results = $query->execute();

	$dealers = [];
	
	if(isset($results['node'])) {
		$node_nids = array_keys($results['node']);
		$nodes = entity_load('node', $node_nids);
		
		/* looping through all results and creating object */
		foreach($nodes as $node) {
			$dealer =  entity_metadata_wrapper('node', $node);

			/* creating address array for use */
			$address = [];
			if(!empty($dealer->field_location_address->value())) {
				/* if proper address field have been used */
				$addressData = $dealer->field_location_address;
				if(!empty($addressData->organisation_name->value())) { $address[] = $addressData->organisation_name->value(); }
				if(!empty($addressData->thoroughfare->value())) { $address[] = $addressData->thoroughfare->value(); }
				if(!empty($addressData->premise->value())) { $address[] = $addressData->premise->value(); }
				if(!empty($addressData->locality->value())) { $address[] = $addressData->locality->value(); }
				if(!empty($addressData->administrative_area->value())) { $address[] = $addressData->administrative_area->value(); }
				if(!empty($addressData->postal_code->value())) { $address[] = $addressData->postal_code->value(); }
				if(!empty($addressData->country->value())) { $address[] = country_load($addressData->country->value())->name; }
				
				foreach($address as $key => $value) { if(empty($value)) { unset($address[$key]); } }
				
				array_filter($address);

			} elseif(!empty($dealer->field_legacy_address->value())) {
				/* try to parce the string to array */
				$explodedArray = multiExplode(
						['</br>', '\r\n', '<br>', '<br />', '\n', PHP_EOL, ', '],
						str_replace(
							['<p>', '</p>'],
							NULL,
							$dealer->field_legacy_address->value()['safe_value']
						)
					);
				$address = array_filter($explodedArray);
			}

			/* if address is invalid or empty current dealer location is unpublished and skipped */
//			if(empty($address)) {
//				$dealer->status = 0;
//				$dealer->save();
//				continue;
//			}

			/* updating dealer locations geometry if it's empty */
			if(!empty($dealer->field_location_data->value())) {
				$geometry = [
					$dealer->field_location_data->lat->value(),
					$dealer->field_location_data->lon->value(),
				];

			} elseif(!empty($dealer->field_dealer_location->value())) {
				/* loads data from overwriten coordinates */
				$geometry = [
					$dealer->field_dealer_location->lat->value(),
					$dealer->field_dealer_location->lon->value(),
				];

			} elseif(!empty($address) && empty($dealer->field_location_data->value()) && empty($dealer->field_dealer_location->value())) {
				/* getting string of address */
				$location = $geocode->get(implode(', ', $address));

				/* if geocoding did not fail save data to field...  */
				if(!empty($location->getLongitude()) && !empty($location->getLongitude())) {
					/* preping geofield data for saving.. */
					$geofield_data = geofield_compute_values([
						'lat' => $location->getLatitude(),
						'lon' => $location->getLongitude(),
					], GEOFIELD_INPUT_LAT_LON);

					$dealer->field_location_data->set($geofield_data);
					$dealer->save();

					$geometry = [
						$dealer->field_location_data->lat->value(),
						$dealer->field_location_data->lon->value(),
					];
				}
			}

			/* if geometry is empty put dealer location unpublished */
//			if(empty($geometry)) {
//				$dealer->status = 0;
//				$dealer->save();
//				continue;
//			}

			/* Dealer Type */
			$storeType = [];
			if(!empty($dealer->field_store_type->value())) {
				foreach($dealer->field_store_type as $type) {
					/* getting parent array*/
					foreach($type->parent->getIterator() as $delta => $parent) {
						$storeType[$parent->tid->value()] = [
							'name' => $parent->name->value(),
							'tid' => $parent->tid->value(),
						];
					}
				}

				foreach($dealer->field_store_type as $type) {
					$storeType[reset($type->parent->value())->tid]['types'][] = [
						'name' => $type->name->value(),
						'tid' => $type->tid->value(),
					];
				}

				$storeType = array_values($storeType);
			}

			/* contact information */
			$contact = [
				'tel' => $dealer->field_telephone->value(),
				'fax' => $dealer->field_fax->value(),
				'email' => $dealer->field_email->value(),
			];

			/* website */
			$website = [
				'url' => !empty($dealer->field_website->value()) ? $dealer->field_website->url->value() : '',
			];

			/* defining dealer */
			$dealers[] = [
				'name' => $dealer->title->value(),
				'address' => array_values($address),
				'coordinates' => $geometry,
				'storeTypes' => $storeType,
				'contact' => $contact,
				'website' => $website,
				'logo' => !empty($dealer->field_logo->value()) ? image_style_url('dealer_logo', $dealer->field_logo->value()['uri']) : NULL,
				'enhanced' => ($dealer->field_enable_store_page->value() == TRUE) ? 1 : 0,
				'url' => drupal_get_path_alias('node/' . $dealer->nid->value()),
				'nid' => $dealer->nid->value(),
			];
		}
	}

	$json = [
		'data' => $dealers,
	];

	echo html_entity_decode(json_encode($json, JSON_UNESCAPED_UNICODE));
}

?>