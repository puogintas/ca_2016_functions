<?php

/* NEW Modal Callback */
function ca_2016_modal_get_modal($nid) {
	$node = node_load($nid);
	return node_view($node, 'modal');
}

function ca_2016_modal_callback($page_callback_result) {
	$content = drupal_render($page_callback_result);
	print $content;
	drupal_page_footer();
}

?>