<?php

function ca_2016_functions_menu() {
	
	/* Universal NODE modal Menu callback */
	$items['node/get/modal/%'] = [
		'page callback' => 'ca_2016_modal_get_modal', // Render HTML
		'page arguments' => [
			3
		],
		'type' => MENU_CALLBACK,
		'access arguments' => [
			'access content'
		],
		'delivery callback' => 'ca_2016_modal_callback',  // Magic goes here
	];
	
	/* block callback */
	$items['modal/block/%'] = [
		'page callback' => 'block_modal', // Render HTML
		'page arguments' => [
			2
		],
		'type' => MENU_CALLBACK,
		'access arguments' => [
			'access content'
		],
		'delivery callback' => 'block_modal_callback',  // Magic goes here
	];
	
	/* Contact us JSON file */
	$items['contact_us/json'] = [
		'page callback' => 'ca_contact_us_json', // Render HTML
		'type' => MENU_CALLBACK,
		'access arguments' => ['access content'],
		'delivery callback' => 'drupal_json_output',  // Magic goes here
	];
	
	/* Zendesk search API callback */
	$items['zendesk/search/%'] = [
		'page callback' => 'zendesk_render', // Render HTML
		'page arguments' => [
			2
		],
		'type' => MENU_CALLBACK,
		'access arguments' => [
			'access content'
		],
		'delivery callback' => 'drupal_json_output'
	];
	
	/* All dealers json */
	$items['dealers/v2/json'] = [
		'page callback' => 'dealers_json_render',
		'access callback' => TRUE,
		'type' => MENU_CALLBACK,
	];

    /* All dealers json */
	$items['retailers/v3/json/super/secret/%'] = [
		'page callback' => 'dealers_json_render_special',
		'access callback' => TRUE,
		'type' => MENU_CALLBACK,
        'page arguments' => [
			5
		],
	];
	
	/* All dealers json */
	$items['front/v2/json'] = [
		'page callback' => 'front_json_render',
		'access callback' => TRUE,
		'type' => MENU_CALLBACK,
	];

	return $items;
	
}

function ca_contact_us_json() {
	$view = views_get_view('json_output');
	$view->set_display('distributors');
	$view->pre_execute();
	$view->execute();
	return $view->render();
}

?>