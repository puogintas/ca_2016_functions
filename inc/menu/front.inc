<?php

/**
* render front page json data
* @private
* @method front_json_render
* @return {Object} description
*/
function front_json_render() {
	global $language;
	
	drupal_add_http_header('Content-Type', 'application/json; charset=utf-8;');
	
	/* create entity query object */
	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type', 'node');
	$query->propertyCondition('status', NODE_PUBLISHED);
	
	/* getting blog posts */
	$query->entityCondition('bundle', ['blog_post', 'custom_tiles']);
	$blog_results = $query->execute();
	
	/* getting blog posts */
	$query->entityCondition('bundle', ['product_new']);
	$query->fieldCondition('field_tile_visibility', 'value', '0', '!=');
	$product_results = $query->execute();
	
	/* merging all results into 1 array */
	$results['node'] = $blog_results['node'] + $product_results['node'];
	
	$data = [];
	
	if(isset($results['node'])) {
		$node_nids = array_keys($results['node']);
		$nodes = entity_load('node', $node_nids);
		
		foreach($nodes as $node) {
			/* wrapping node into entity metadata wrapper for easier data handling */
			$wrapper = entity_metadata_wrapper('node', $node);
			
			if($wrapper->field_tile_image->value()) {
				$path = file_create_url($wrapper->field_tile_image->value()['uri']);
				$pathProcessed = $wrapper->field_tile_image->value()['filemime'] == 'image/gif' ? file_create_url($wrapper->field_tile_image->value()['uri']) : image_style_url('tile__horizontal_rectangle', $wrapper->field_tile_image->value()['uri']);
			}
			
			$url = $wrapper->type->value() != 'custom_tiles' ? url('node/' . $wrapper->getIdentifier()) : url($wrapper->field_link_to_content->url->value());
			
			$data[] = (object) [
				'title' => isset($wrapper->field_n_display_title) && !empty($wrapper->field_n_display_title->value()) ? $wrapper->language($language->language)->field_n_display_title->value() : $wrapper->language($language->language)->title->value(),
				'strapline' => isset($wrapper->field_n_strapline) && !empty($wrapper->field_n_strapline->value()) ? $wrapper->language($language->language)->field_n_strapline->value() : '',
				'type' => $wrapper->type->value() == 'blog_post' && $wrapper->field_blog_cat->value() == 4 ? 'testimonial' : $wrapper->type->value(),
				'image' => $path ? $path : NULL,
				'imageProcessed' => $pathProcessed ? $pathProcessed : '',
				'weight' => $wrapper->field_tile_weight->value(),
				'created' => $wrapper->created->value(),
				'changed' => $wrapper->changed->value(),
				'blogCategory' => $wrapper->type->value() == 'blog_post' ? $wrapper->field_blog_cat->label() : '',
				'url' => $url,
				'score' => $wrapper->field_pageview_count->value(),
			];

		}
	}
	
	/* sorting data based by pageviews */
	$pageviews = [];
	foreach ($data as $key => $row) { $pageviews[$key] = $row->score; }
	array_multisort($pageviews, SORT_DESC, $data);
	
	$json = [
		'data' => $data,
	];
	
	/* gets all dealer json file*/
	echo html_entity_decode(json_encode($json, JSON_UNESCAPED_UNICODE));;
	
	/* returns null making page cacheble */
	return NULL;
}

/**
 * Initializes an Analytics Reporting API V4 service object.
 *
 * @return An authorized Analytics Reporting API V4 service object.
 */
function initializeAnalytics() {

	/* Use the developers console and download your service account credentials in JSON format. Place them in this directory or change the key file location if necessary. */
	$KEY_FILE_LOCATION = '/home/ca/public_html/sites/all/modules/ca_2016_functions/inc/menu/keys/CambridgeAudio-e12a56c32226.json';
//	$KEY_FILE_LOCATION =  __DIR__ . '/keys/CambridgeAudio-e12a56c32226.json';

	/* Create and configure a new client object. */
	$client = new Google_Client();
	$client->setApplicationName("Front Page Reporting");
	$client->setAuthConfig($KEY_FILE_LOCATION);
	$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	$analytics = new Google_Service_AnalyticsReporting($client);

	return $analytics;
}


/**
 * Queries the Analytics Reporting API V4.
 *
 * @param service An authorized Analytics Reporting API V4 service object.
 * @return The Analytics Reporting API V4 response.
 */
function getReport($analytics, $path = NULL, $external = FALSE) {

	/* Replace with your view ID, for example XXXX. */
	$VIEW_ID = "73435267";

	/* Create the DateRange object. */
	$dateRange = new Google_Service_AnalyticsReporting_DateRange();
	$dateRange->setStartDate('30daysAgo');
	$dateRange->setEndDate('today');
	
	/* Create the Dimensions object. */
	$dimension = new Google_Service_AnalyticsReporting_Dimension();
	$dimension->setName('ga:pagePath');

	/* Create the Metrics object. */
	$metrics = new Google_Service_AnalyticsReporting_Metric();
	$metrics->setExpression('ga:pageviews');
	
	/* Seting Segment dimmenisons (path) */
	if($external == FALSE) {
		$path = 'www.cambridgeaudio.com' . $path;
	}
	
	/* Create the DimensionFilter. */
	$dimensionFilter = new Google_Service_AnalyticsReporting_DimensionFilter();
	$dimensionFilter->setDimensionName('ga:pagePath');
	$dimensionFilter->setOperator('EXACT');
	$dimensionFilter->setExpressions([$path]);

	/* Create the DimensionFilterClauses */
	$dimensionFilterClause = new Google_Service_AnalyticsReporting_DimensionFilterClause();
	$dimensionFilterClause->setFilters([$dimensionFilter]);
	
	/* Create the ReportRequest object. */
	$request = new Google_Service_AnalyticsReporting_ReportRequest();
	$request->setViewId($VIEW_ID);
	$request->setDimensions([$dimension]);
	$request->setMetrics([$metrics]);
	$request->setDimensionFilterClauses([$dimensionFilterClause]);
	$request->setDateRanges($dateRange);
	
	/* Getting Response */
	$body = new Google_Service_AnalyticsReporting_GetReportsRequest();
	$body->setReportRequests([$request]);
	
	return $analytics->reports->batchGet($body);
}


/**
 * Parses and prints the Analytics Reporting API V4 response and returns xact pageviews
 *
 * @param An Analytics Reporting API V4 response.
 */
function getResults($reports) {
	
	for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
		$report = $reports[ $reportIndex ];
		$header = $report->getColumnHeader();
		$dimensionHeaders = $header->getDimensions();
		$metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
		$rows = $report->getData()->getRows();

		for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
			$row = $rows[ $rowIndex ];
			$dimensions = $row->getDimensions();
			$metrics = $row->getMetrics();

			for ($j = 0; $j < count($metrics); $j++) {
				$values = $metrics[$j]->getValues();
				for ($k = 0; $k < count($values); $k++) {
					$entry = $metricHeaders[$k];
					return($values[$k]);
				}
			}
		}
	}
	
}

?>