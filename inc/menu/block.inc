<?php

function block_modal($block_id) {
	$block = module_invoke('block', 'block_view', $block_id);
	return $block['content'];
}

function block_modal_callback($page_callback_result) {
	print $page_callback_result;
	drupal_page_footer();
}

?>