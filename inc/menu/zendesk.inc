<?php

function zendesk_render($search) {
	$search = urlencode($search);
	$postOptions = array(
        'method' => 'GET',
        'timeout' => 30,
        'max_redirects' => 3,
        'headers' => array(
            'Content-Type' => 'application/json',
            'Authorization' => 'Basic '. drupal_base64_encode('mark.pennock@cambridgeaudio.com/token:YmJDE2Ob0k71nnpYtbNae5nomlL1ENlrUz1B9PA4'),
        ),
    );

	$postResult = drupal_http_request('https://audiopartnership.zendesk.com/api/v2/help_center/articles/search.json?query=' . $search, $postOptions);

	$data = json_decode($postResult->data);

	return $data;
}

?>