<?php

/**
* Altering Dealer edit form for BDMs
* @private
* @method ca_2016_functions_form_dealer_node_form_alter
* @param {Object} &$form
* @param {Object} &$form_state
* @param {Object} $form_id
* @return {Object} description
*/
function ca_2016_functions_form_dealer_node_form_alter(&$form, &$form_state, $form_id) {
	global $user;
	
	if(in_array('BDM', $user->roles)) {
		
		$form['#attached']['css'][] = drupal_get_path('module', 'ca_2016_functions') . '/inc/bdm.css';
		
		$account = entity_metadata_wrapper('user', user_load($user->uid));
		
		/* Only showing availalbe countries for Dealer Location edit / add */
		$countries = [];
		if(!empty($account->field_available_countries->value())) {
			foreach($account->field_available_countries as $country) { $countries[$country->iso2->value()] = $country->name->value(); }
		}
		$form['field_location_address']['und'][0]['country']['#options'] = $countries;
		
		/* Hide geocoded location if its empty and disable it if its not */
		if(empty($form['field_location_data']['und'][0]['geom']['#default_value'])) { $form['field_location_data']['#access'] = FALSE; }
		else { $form['field_location_data']['#disabled'] = TRUE; }
		
		$form['field_dealer_location']['und'][0]['#description'] = '';
		$form['field_dealer_location']['und'][0]['geom']['#title'] = '';
		$form['field_legacy_address']['#disabled'] = TRUE;
		
		$form['options']['#access'] = TRUE;
		$form['options']['promote']['#access'] = FALSE;
		$form['options']['sticky']['#access'] = FALSE;
	}
    
    if(in_array('distributor', $user->roles)) {
		$form['#attached']['css'][] = drupal_get_path('module', 'ca_2016_functions') . '/inc/bdm.css';
		
		$account = entity_metadata_wrapper('user', user_load($user->uid));
		
		/* Only showing availalbe countries for Dealer Location edit / add */
		$countries = [];
		if(!empty($account->field_available_countries->value())) {
			foreach($account->field_available_countries as $country) {
                $countries[$country->iso2->value()] = $country->name->value();
            }
		}
		$form['field_location_address']['und'][0]['country']['#options'] = $countries;
		
		/* Hide geocoded location if its empty and disable it if its not */
		if(empty($form['field_location_data']['und'][0]['geom']['#default_value'])) {
            $form['field_location_data']['#access'] = FALSE;
        } else {
            $form['field_location_data']['#disabled'] = TRUE;
        }
		
		$form['field_dealer_location']['und'][0]['#description'] = '';
		$form['field_dealer_location']['und'][0]['geom']['#title'] = '';
		
        /* hiding legacy address */
        $form['field_legacy_address']['#access'] = FALSE;
		
        /* setting up custom access */
		$form['options']['#access'] = TRUE;
		$form['options']['promote']['#access'] = FALSE;
		$form['options']['sticky']['#access'] = FALSE;
		
        $form['actions']['submit']['#value'] = 'Save & Publish';
        $form['actions']['submit']['#submit'][] = 'retailer_callback';
	}
	
}


/**
* redirecting to user page after user edit
* @private
* @method retailer_callback
* @param {Object} $form
* @param {Object} $form_state
* @return {Object} description
*/
function retailer_callback($form, $form_state) {
	drupal_goto('distributors');
}

/**
* altering profile form
* @private
* @method ca_2016_functions_form_user_profile_form_alter
* @param {Object} &$form
* @param {Object} &$form_state
* @param {Object} $form_id
* @return {Object} description
*/
function ca_2016_functions_form_user_profile_form_alter(&$form, &$form_state, $form_id) {
    global $user;
    
    if(in_array('distributor', $user->roles)) {
        $form['#attached']['css'][] = drupal_get_path('module', 'ca_2016_functions') . '/inc/bdm.css';
        
        $form['field_bdm_responsible']['#access'] = FALSE;
        $form['field_available_countries']['#access'] = FALSE;
        $form['signature_settings']['#access'] = FALSE;
        $form['picture']['#access'] = FALSE;
        $form['#metatags']['#access'] = FALSE;
        
        $form['actions']['submit']['#submit'][] = 'distributor_edit_callback';
    }
}


/**
* redirecting to user page after user edit
* @private
* @method distributor_edit_callback
* @param {Object} $form
* @param {Object} $form_state
* @return {Object} description
*/
function distributor_edit_callback($form, $form_state) {
	drupal_goto('user');
}

?>