<?php

function ca_2016_functions_page_build(&$page) {
	global $language;

	// default variables // cached
	$variables['language'] = $language->language;
	$variables['trans'] = [
		'error' => [
			'empty' => t('Please type a keyword in order to perform the search.'),
			'no_results' => t("Unfortunately, we couldn't find any articles matching your search criteria.") . ' ' . t('If you have a product usage related query, have you tried searching our <a href="https://techsupport.cambridgeaudio.com/hc/en-us">Product Support</a> pages?'),
		],
		'miles' => t('miles'),
		'findMap' => t('Find on map'),
		'viewWebsite' => t('View website'),
		'viewStorePage' => t('Visit store page'),
		'directions' => t('Directions'),
		'back' => t('back'),
	];
	drupal_add_js($variables, 'setting');
	
	//GEOLOCATION
	$geolocation = [
		'CF_IPCOUNTRY' => $_SERVER['HTTP_CF_IPCOUNTRY'],
	];
	$geoParameters = [
		'type' => 'setting',
		'cache' => FALSE,
		'preprocess' => FALSE,
		'weight' => -99,
	];
	
	drupal_add_js($geolocation,$geoParameters);
}

?>