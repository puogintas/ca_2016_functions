<?php

/* rewrite of image_style default function to not include width and height */
function ca_2016_functions_theme_image_style($variables) {
	$variables['width'] = NULL;
	$variables['height'] = NULL;
	$variables['path'] = image_style_url($variables['style_name'], $variables['path']);
	return theme('image', $variables);
}

/* to output image without any styles */
function ca_2016_functions_original_image($class, $path) {
	return '<img class="' . $class . '" src="' . $path . '" />';
}

function ca_lightbox($uri, $class, $thumb_style, $large_style) {
	$thumBconfig = array(
		'style_name' => $thumb_style,
		'path' => $uri,
		'attributes' => array(
			'class' => 'responsive-image'
		)
	);
	$thumbImage = ca_2016_functions_theme_image_style($thumBconfig);
	$largeURL = image_style_url($large_style, $uri);
	$output = '<a href="'. $largeURL . '" class="'. $class . '">' . $thumbImage . '</a>';
	
	return $output;
}

function ca_image($uri, $class = NULL, $thumb_style, $alt = NULL) {
	$thumBconfig = array(
		'style_name' => $thumb_style,
		'path' => $uri,
		'alt' => !empty($alt) ? $alt : NULL,
		'attributes' => array(
			'class' => !empty($class) ? $class : NULL,
		)
	);
	$thumbImage = ca_2016_functions_theme_image_style($thumBconfig);
	
	return $thumbImage;
}

?>