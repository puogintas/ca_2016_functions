<?php

function langprefix($language) {
	return ($language->language != 'en') ? '/' . $language->prefix . '/' : '/';
}

function json_output($page_callback_result) {
	drupal_add_http_header('Content-Type', 'application/json');
	echo json_encode($page_callback_result, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
}

/*
Name: get_products_array
Description: gets an array of all products + sorted by price
returns: array
*/
function get_products_array($range_id = NULL) {

	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type', 'node');
	$query->entityCondition('bundle', 'product_new');
	$query->propertyCondition('status', NODE_PUBLISHED);
	$query->fieldCondition('field_is_accessory', 'value', '1', '!=');
	$query->fieldCondition('field_archive', 'value', '1', '!=');
	if(!empty($range_id)) {
		$query->fieldCondition('field_belongs_to_range', 'target_id', $range_id, '=');
	}
	$result = $query->execute();

	$products = [];

	foreach($result['node'] as $key => $data) {
		$product = node_load($key);
		$price = !empty($product->field_price) ? intval(preg_replace("/[^0-9]/","",$product->field_price['und'][0]['value'])) : 0;
		$products[] = [
			'nid' => $key,
			'title' => $product->title,
			'value' => $price,
		];
	}

	usort($products, function($a, $b) {
		if($a['value']==$b['value']) return 0;
		return $a['value'] < $b['value']?1:-1;
	});
	
	return $products;
}

/*
Name: get_hero_id
Description: gets current slider hero node id
returns: integer
*/
function get_hero_id($term) {
	if($term->vid == 22 || $term->vid == 23) {
		$query = new EntityFieldQuery();
		$query->entityCondition('entity_type', 'node');
		$query->entityCondition('bundle', 'slider');
		$query->propertyCondition('status', NODE_PUBLISHED);

		/* Checks if current $term is Category */
		if($term->vid == 22) { $query->fieldCondition('field_appear_category', 'target_id', $term->tid, '='); }

		/* Checks if current $term is Range */
		elseif($term->vid == 23) { $query->fieldCondition('field_range_', 'target_id', $term->tid, '='); }

		$result = $query->execute();
		$result = reset($result['node']);
	}
	
	return !empty($result) ? $result->nid : NULL;
}

/*
Name: error_email
Description: send Drupal email using default drupal system.
 - $to - email address to send email
 - $subject - subject of the email
 - $message - long html message
 - $from - (optional) from address
returns: TRUE or FALSE
*/
function error_email($to, $subject, $message, $from = 'default_from') {
	/* Change this to your own default 'from' email address. */
	if($from == 'default_from') {
		$from = 'Cambridge Audio <webadmin@cambridgeaudio.com>';
	}
	
	$message = [
		'id' => 'ca_2016_functions_' . microtime(),
		'module' => 'ca_2016_functions',
		'key' => 'ca_2016_functions_error',
		'to' => $to,
		'from' => $from,
		'subject' => $subject,
		'body' => [$message],
		'headers' => [
			'From' => $from,
			'Sender' => $from,
			'Return-Path' => $from,
			'MIME-Version' => '1.0',
			'Content-Type' => 'text/html; charset=UTF-8',
		],
	];
	
	$system = drupal_mail_system('ca_2016_functions', microtime());
	$message = $system->format($message);
	
	if($system->mail($message)) { return TRUE; }
	else { return FALSE; }
}

?>