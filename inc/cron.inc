<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception;

/**
* Define all cron jobs needed for CA website
* @private
* @method ca_2016_functions_cronapi
* @param {Object} $op
* @param {Object} $job = NULL
* @return [array] $items - cron jobs
*/
function ca_2016_functions_cronapi($op, $job = NULL) {
	
	$items['get_b2c_link_us'] = [
		'description' => 'Sync CA B2C US product prices + URL',
		'rule' => '0 */12 * * *',
	];
	
	$items['update_node_pageviews'] = [
		'description' => 'Updates pages pageview counts',
		'rule' => '0 0 */5 * *',
	];
	
	return $items;
}

/**
* updates node pageviews counters....
* @private
* @method update_node_pageviews
* @return {Object} description
*/
function update_node_pageviews() {
	
	/* create entity query object */
	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type', 'node');
	$query->propertyCondition('status', NODE_PUBLISHED);
	$query->entityCondition('bundle', ['blog_post', 'custom_tiles', 'product_new']);
	$results = $query->execute();
	
	/* initializing Google Analytics client */
	$analytics = initializeAnalytics();
	
	if(isset($results['node'])) {
		$node_nids = array_keys($results['node']);
		$nodes = entity_load('node', $node_nids);
		
		foreach($nodes as $node) {
			/* wrapping node into entity metadata wrapper for easier data handling */
			$wrapper = entity_metadata_wrapper('node', $node);
			$url = $wrapper->type->value() != 'custom_tiles' ? url('node/' . $wrapper->getIdentifier()) : url($wrapper->field_link_to_content->url->value());
			
			/* get analytics report  */
			$report = getReport($analytics, $url);
			$pageViews = getResults($report);
			
			/* save data */
			$wrapper->field_pageview_count = !empty($pageViews) ? $pageViews : 0;
			$wrapper->save();
		}
	}
		
	
	watchdog('update_node_pageviews', 'Update compleeted');
}


/**
* Processes prices and URL from B2C US shop
* @private
* @method get_b2c_link_us
*/
function get_b2c_link_us() {
	
	/* guzzle HTTP call */
	try {
		/* Create a client with a base URI */
		$client = new GuzzleHttp\Client([
			'base_uri' => 'https://shop.cambridgeaudio.com/us/json/v1/',
			'headers' => [
				'Content-Type' => 'application/json',
			],
		]);

		/* initiate request object */
	
		$response = $client->request('GET', 'prices', [
			'curl' => [
				CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1
			]
		]);
		
		/* get and parse response object */
		$data = json_decode($response->getBody()->getContents());
		
	} catch (\Exception $e) {
		
		/* email error message */
		$error = Psr7\str($e->getResponse());
		error_email(
			'povilas.uogintas@cambridgeaudio.com',
			'Error while updating products (B2C US)', 'Hey,<br><br>Error has occured:<br><br>' . $error . '<br><br>Cheers,<br>Cambridge Audio'
		);
		
		$message = 'Update failed. Please refer to email';
    }
	
	if(!empty($data)) {
		$counters = [
			'updated' => 0,
			'skipped' => 0,
		];

		foreach($data->products as $product) {
			if(!empty($product)) {
				/* get node by UniqueID */
				$query = new EntityFieldQuery();
				$query->entityCondition('entity_type', 'node');
				$query->entityCondition('bundle', 'product_new');
				$query->propertyCondition('status', NODE_PUBLISHED);
				$query->fieldCondition('field_uniqueid', 'value', $product->product->UniqueID, '=');
				
				$results = $query->execute();
				
				/* check if node has been returned */
				if(!empty($results)) {
					$results = reset($results);
					$results = reset($results);

					$product_node = entity_metadata_wrapper('node', node_load($results->nid));
					$product_node->field_price_usa = $product->product->Price;
					$product_node->field_link_to_marketplace_usa->url = 'https://shop.cambridgeaudio.com' . $product->product->Path;
					$product_node->save();

					$counters['updated']++;
				} else {
					$counters['skipped']++;
				}
			} else {
				$counters['skipped']++;
			}
		}
		
		$message = 'B2C US update cron job has been performed. Updated: ' . $counters['updated'] . '. Skipped: ' . $counters['skipped'];
		
	} else {
		$message = 'Update failed. Please refer to email';
	}

	watchdog('get_b2c_link_us', $message);
}

?>